//
//  MembersViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "MembersViewController.h"

@interface MembersViewController ()

@end

@implementation MembersViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.membersView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 700)];
    [self setScrollView];
}

- (void)setScrollView
{
    UIScrollView *membersScrollView = (UIScrollView *)self.membersScrollView;
    membersScrollView.contentSize=CGSizeMake(self.membersView.frame.size.width, self.membersView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setMembersView:nil];
    [self setMembersScrollView:nil];
    [super viewDidUnload];
}
@end
