//
//  TraineeViewController.h
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TraineeViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *traineeView;
@property (weak, nonatomic) IBOutlet UIScrollView *traineeScrollView;

@end
