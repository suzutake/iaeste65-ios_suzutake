//
//  TeddybearsViewController.h
//  IAESTE65
//
//  Created by takerukun on 13/10/06.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeddybearsViewController : UIViewController


@property (strong, nonatomic) IBOutlet UIView *teddyView;
@property (weak, nonatomic) IBOutlet UIScrollView *teddyScrollview;

@end
