//
//  competitionsViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "competitionsViewController.h"

@interface competitionsViewController ()

@end

@implementation competitionsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.competitionView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 1500)];
    [self setScrollView];
}

- (void)setScrollView
{
    UIScrollView *competitionScrollView = (UIScrollView *)self.competitionScrollView;
    competitionScrollView.contentSize=CGSizeMake(self.competitionView.frame.size.width, self.competitionView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setCompetitionView:nil];
    [self setCompetitionScrollView:nil];
    [super viewDidUnload];
}
@end
