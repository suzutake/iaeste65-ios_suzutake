//
//  StudentsViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "StudentsViewController.h"

@interface StudentsViewController ()

@end

@implementation StudentsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.studentsView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 800)];
    [self setScrollView];
}

- (void)setScrollView
{
    UIScrollView *studentScrollView = (UIScrollView *)self.studentScrollView;
    studentScrollView.contentSize=CGSizeMake(self.studentsView.frame.size.width, self.studentsView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setStudentsView:nil];
    [self setStudentScrollView:nil];
    [super viewDidUnload];
}
@end
