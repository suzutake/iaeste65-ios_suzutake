//
//  EventViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/06.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "EventViewController.h"

@interface EventViewController ()

@end

@implementation EventViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.eventView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 1600)];
	[self setScrollView];
    //set email button function
    [self.sendEMailButton addTarget:self action:@selector(sendEMail) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setScrollView
{
    UIScrollView *eventScrollView = (UIScrollView *)self.eventScrollView;
    eventScrollView.contentSize=CGSizeMake(self.eventView.frame.size.width, self.eventView.frame.size.height);
}
     
-(void)sendEMail
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:gst@iaeste.org"]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setEventScrollView:nil];
    [self setEventView:nil];
    [self setSendEMailButton:nil];
    [super viewDidUnload];
}
@end
