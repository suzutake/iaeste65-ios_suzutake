//
//  DayViewController.h
//  IAESTE65
//
//  Created by takerukun on 13/10/06.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DayViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *dayView;
@property (weak, nonatomic) IBOutlet UIScrollView *dayScrollView;
@end
