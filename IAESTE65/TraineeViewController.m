//
//  TraineeViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "TraineeViewController.h"

@interface TraineeViewController ()

@end

@implementation TraineeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.traineeView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 700)];
    [self setScrollView];
}

- (void)setScrollView
{
    UIScrollView *traineeScrollView = (UIScrollView *)self.traineeScrollView;
    traineeScrollView.contentSize=CGSizeMake(self.traineeView.frame.size.width, self.traineeView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTraineeView:nil];
    [self setTraineeScrollView:nil];
    [super viewDidUnload];
}
@end
