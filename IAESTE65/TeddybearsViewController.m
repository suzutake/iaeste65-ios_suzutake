//
//  TeddybearsViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/06.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "TeddybearsViewController.h"

@interface TeddybearsViewController ()

@end

@implementation TeddybearsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.teddyView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 1536)];
    [self setScrollView];
}

- (void)setScrollView
{
    UIScrollView *teddyScrollView = (UIScrollView *)self.teddyScrollview;
    teddyScrollView.contentSize=CGSizeMake(self.teddyView.frame.size.width, self.teddyView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTeddyScrollview:nil];
    [self setTeddyView:nil];
    [super viewDidUnload];
}
@end
