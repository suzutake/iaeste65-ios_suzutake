//
//  ViewController.h
//  IAESTE65
//
//  Created by Patrick Wijerama on 8/7/13.
//  Copyright (c) 2013 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextView *mainTextView01;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@end
