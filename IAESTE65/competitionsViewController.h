//
//  competitionsViewController.h
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface competitionsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *competitionView;
@property (weak, nonatomic) IBOutlet UIScrollView *competitionScrollView;

@end
