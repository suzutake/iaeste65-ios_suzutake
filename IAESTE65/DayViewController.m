//
//  DayViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/06.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "DayViewController.h"

@interface DayViewController ()

@end

@implementation DayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.dayView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 500)];
	[self setScrollView];
}

- (void)setScrollView
{
    UIScrollView *dayScrollView = (UIScrollView *)self.dayScrollView;
    dayScrollView.contentSize=CGSizeMake(self.dayView.frame.size.width, self.dayView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDayView:nil];
    [self setDayScrollView:nil];
    [self setDayView:nil];
    [super viewDidUnload];
}
@end
