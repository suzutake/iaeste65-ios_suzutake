//
//  ViewController.m
//  IAESTE65
//
//  Created by Patrick Wijerama on 8/7/13.
//  Copyright (c) 2013 Patrick Wijerama. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self setScrollView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    [self setScrollView];
}

- (void)setScrollView
{
    UIScrollView *mainScrollView = (UIScrollView *)self.mainScrollView;
    mainScrollView.contentSize=CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
}

-(void)setTextViews
{
    self.mainTextView01.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth;
}

-(void)layoutPortrait
{
    self.mainTextView01.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}


- (void)viewDidUnload {
    [self setMainScrollView:nil];
    [self setMainTextView01:nil];
    [super viewDidUnload];
}
@end
