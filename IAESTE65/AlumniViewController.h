//
//  AlumniViewController.h
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlumniViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *alumniView;
@property (weak, nonatomic) IBOutlet UIScrollView *alumniScrollView;
@property (weak, nonatomic) IBOutlet UIButton *linkButton;

@end
