//
//  DanceViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/06.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "DanceViewController.h"

@interface DanceViewController ()

@end

@implementation DanceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.danceView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 600)];
	[self setScrollView];
}
- (void)setScrollView
{
    UIScrollView *danceScrollView = (UIScrollView *)self.danceScrollView;
    danceScrollView.contentSize=CGSizeMake(self.danceView.frame.size.width, self.danceView.frame.size.height);
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setDanceScrollView:nil];
    [self setDanceView:nil];
    [super viewDidUnload];
}
@end
