//
//  StudentsViewController.h
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StudentsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIView *studentsView;
@property (weak, nonatomic) IBOutlet UIScrollView *studentScrollView;

@end
