//
//  AlumniViewController.m
//  IAESTE65
//
//  Created by takerukun on 13/10/07.
//  Copyright (c) 2013年 Patrick Wijerama. All rights reserved.
//

#import "AlumniViewController.h"

@interface AlumniViewController ()

@end

@implementation AlumniViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.alumniView= [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 700)];
    [self setScrollView];
    [self.linkButton addTarget:self action:@selector(openWebPage) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setScrollView
{
    UIScrollView *alumniScrollView = (UIScrollView *)self.alumniScrollView;
    alumniScrollView.contentSize=CGSizeMake(self.alumniView.frame.size.width, self.alumniView.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)openWebPage
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://65th.iaeste.org/competitions/alumni-story-competition/how-to-enter/"]];
}

- (void)viewDidUnload {
    [self setAlumniView:nil];
    [self setAlumniScrollView:nil];
    [self setLinkButton:nil];
    [super viewDidUnload];
}
@end
